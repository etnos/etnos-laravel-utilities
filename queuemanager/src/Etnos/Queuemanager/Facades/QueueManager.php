<?php namespace Etnos\Queuemanager\Facades;

use Illuminate\Support\Facades\Facade;

class QueueManager extends Facade {
	protected static function getFacadeAccessor() { return 'queuemanager'; }	
}
?>