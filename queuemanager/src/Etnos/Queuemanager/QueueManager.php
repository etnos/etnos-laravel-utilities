<?php namespace Etnos\Queuemanager;

use Queue;
use Request;

/**
 * Class for Laravel Queue Managing
 * @author ruca
 */

class QueueManager {

	function push($jobTitle,$data,$queueTitle,$limit = 5) {

		if(!isset($data['ip']))
			return false;

		$jobsList = (Queue::getRedis()->command('LRANGE',['queues:'.$queueTitle, '0', '-1']));
		$jobCount = 0;
		$ipCount = 0;

		foreach($jobsList as $job) {
			if(strcmp(json_decode($job)->job, $jobTitle) == 0 && $data['ip'] == Request::getClientIp())
				$jobCount++;
		}
		
		if($jobCount < $limit) {
			Queue::push($jobTitle,$data, $queueTitle);
			return true;
		}

		return false;

	}

}