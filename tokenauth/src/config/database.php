<?php

return array(
	'users_table' => 'profile',
	'users_identifier' => 'email',
	'users_password' => 'password',
	'users_uuid' => 'uuid'
);