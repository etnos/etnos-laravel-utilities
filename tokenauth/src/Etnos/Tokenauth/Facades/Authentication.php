<?php namespace Etnos\Tokenauth\Facades;

use Illuminate\Support\Facades\Facade;

class Authentication extends Facade {
	protected static function getFacadeAccessor() { return 'authentication'; }	
}
?>