<?php namespace Etnos\Tokenauth;

use Config;
use Request;
use Hash;
use DB;
use Session;
use Cookie;

class Authentication {
	
	function login($identifier,$password) {
		$config = Config::get('tokenauth::database');

		// Find user
	    $query = DB::table($config['users_table']);
	    $query = $query->where($config['users_identifier'], $identifier);
	    $user = $query->first(array($config['users_identifier'], $config['users_password']));

	    if (!Hash::check($password, $user->password)) {
        	return false;
    	}

    	$uuid = Hash::make($identifier.date('Ymd').uniqid());

    	$query->update(array($config['users_uuid'] => $uuid));

    	return $uuid;
	}

	function auth() {
		// Check if auth header was sent
	    if (!Request::header('X-AUTH-KEY')) {
	    	if(!isset($_COOKIE['xtoken'])) {
	    		return false;
	    	}
	    }

	    $config = Config::get('tokenauth::database');
	    // Check if uuid is valid
	    $token = null;

	    if(!Request::header('X-AUTH-KEY')) {
	    	$token = $_COOKIE['xtoken'];
	    }else{
	    	$token = Request::header('X-AUTH-KEY');
	    }

	    $user = DB::table($config['users_table'])->where($config['users_uuid'], $token)->first();

	    if (!$user) {
	        return false;
	    }

	    // Store user in session for later (assumed array sessions)
	    Session::put('user', (array) $user);
	    return true;
	}

}