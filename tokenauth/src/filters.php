<?php

Route::filter('tokenauth', function() {

	$authed = App::make('tokenauth')->auth();

	if($authed == false) {
		return Response::make('Unauthorized', 401);
	}
	
});