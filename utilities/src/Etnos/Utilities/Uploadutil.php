<?php namespace Etnos\Utilities;

use Input;
use Response;
use Validator;
use File;

/**
 * Class for receiving images from POST
 * @author snake
 */

class Uploadutil {

	public function receive($destination) {
		$input = $destination.'imagedata';
		if(!Input::hasFile($input)) {
			return Response::json(['error' => true, 'errors' => 'no input file']);
		}

		$file = Input::file($input);

		$input = array('image' => $file);

		$rules = array(
			'image' => 'image'
			);

		$validator = Validator::make($input, $rules);

		if ( $validator->fails() )
		{

			return array(
				'error' => true,
				'errors' => $validator->getMessageBag()->toArray()
			);

		}
		else {
			$destinationPath = public_path() . '/uploads/'.$destination.'/';
			$extension = File::extension($file->getClientOriginalName());
			$filename = sha1(time().time()).".".$extension;
			$file->move($destinationPath, $filename);
			
			return array(
				'error' => false,
				'file' => $filename
			);
		}
	}
}