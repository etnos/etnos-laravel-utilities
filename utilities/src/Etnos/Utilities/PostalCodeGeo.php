<?php namespace Etnos\Utilities;

/**
 * Class for Geo Information with Google
 * @author snake
 */

class PostalCodeGeo {

	public function getGeo($postal_code) {
		$curl = new CurlComm;
		$url = 'http://maps.google.com/maps/api/geocode/json?address='.$postal_code.'&components=country:PT&sensor=true';
		$output = $curl->requestJson($url);

		print_r($output);
	}

}

?>