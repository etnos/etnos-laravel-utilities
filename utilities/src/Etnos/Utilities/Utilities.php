<?php namespace Etnos\Utilities;

/**
 * Class for integration as a IoC object in Laravel
 * @author snake
 */

class Utilities {

	function time() {
		return new Timeutil;
	}

	function upload() {
		return new Uploadutil;
	}

	function postalGeoService() {
		return new PostalCodeGeo;
	}

}

?>